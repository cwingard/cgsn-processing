#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@package cgsn_processing.process.proc_suna
@file cgsn_processing/process/proc_suna.py
@author Christopher Wingard
@brief Creates a NetCDF dataset for the Nitrate concentration data from the SUNA version of the NUTNR
"""
import numpy as np
import os
import re

from gsw import SP_from_C, p_from_z
from netCDF4 import Dataset
from pocean.utils import dict_update
from pocean.dsg.timeseries.om import OrthogonalMultidimensionalTimeseries as OMTs
from scipy.interpolate import interp1d

from cgsn_processing.process.common import inputs, json2df, df2omtdf
from cgsn_processing.process.finding_calibrations import find_calibration
from cgsn_processing.process.proc_nutnr import Calibrations
from cgsn_processing.process.configs.attr_nutnr import SUNA

from pyseas.data.nit_functions import ts_corrected_nitrate


def main(argv=None):
    # load the input arguments
    args = inputs(argv)
    infile = os.path.abspath(args.infile)
    outfile = os.path.abspath(args.outfile)
    platform = args.platform
    deployment = args.deployment
    lat = args.latitude
    lon = args.longitude
    depth = args.depth
    ctd_name = args.devfile  # name of co-located CTD

    # load the json data file and return a panda dataframe
    df = json2df(infile)
    if df.empty:
        # there was no data in this file, ending early
        return None

    # check for the source of calibration coeffs and load accordingly
    coeff_file = os.path.abspath(args.coeff_file)
    dev = Calibrations(coeff_file)  # initialize calibration class
    if os.path.isfile(coeff_file):
        # we always want to use this file if it exists
        dev.load_coeffs()
    else:
        # load from the CI hosted CSV files
        csv_url = find_calibration('NUTNR', str(df.serial_number[0]), (df.time.values.astype('int64') / 1e9)[0])
        if csv_url:
            dev.read_csv(csv_url)
            dev.save_coeffs()
        else:
            print('A source for the NUTNR calibration coefficients for {} could not be found'.format(infile))
            return None

    # pop the raw_channels array out of the dataframe (will put it back in later)
    channels = np.array(np.vstack(df.pop('channel_measurements')))
    # create the wavelengths array
    wavelengths = dev.coeffs['wl']

    # Merge the co-located CTD temperature and salinity data and calculate the corrected nitrate concentration
    nutnr_path, nutnr_file = os.path.split(infile)
    ctd_file = re.sub('nutnr[\w]*', ctd_name, nutnr_file)
    ctd_path = re.sub('nutnr', re.sub('[\d]*', '', ctd_name), nutnr_path)
    ctd = json2df(os.path.join(ctd_path, ctd_file))
    if not ctd.empty and len(ctd.index) >= 3:
        # The Global moorings may use the data from the METBK-CT for the NUTNR mounted on the buoy subsurface plate.
        # We'll rename the data columns from the METBK to match other CTDs and process accordingly.
        if re.match('metbk', ctd_name):
            # rename temperature and salinity
            ctd = ctd.rename(columns={
                'sea_surface_temperature': 'temperature',
                'sea_surface_conductivity': 'conductivity'
            })
            # set the depth in dbar from the measured depth in m below the water line.
            if re.match('metbk1', ctd_name):
                ctd['pressure'] = p_from_z(-1.3661, lat)
            elif re.match('metbk2', ctd_name):
                ctd['pressure'] = p_from_z(-1.2328, lat)
            else:   # default of 1.00 m
                ctd['pressure'] = p_from_z(-1.0000, lat)

        # calculate the practical salinity of the seawater from the temperature, conductivity and pressure
        # measurements
        ctd['psu'] = SP_from_C(ctd['conductivity'].values * 10.0, ctd['temperature'].values, ctd['pressure'].values)

        # interpolate temperature and salinity data from the CTD into the FLORT record for calculations
        degC = interp1d(ctd.time.values.astype('int64'), ctd.temperature.values, bounds_error=False)
        df['temperature'] = degC(df.time.values.astype('int64'))

        psu = interp1d(ctd.time.values.astype('int64'), ctd.psu, bounds_error=False)
        df['salinity'] = psu(df.time.values.astype('int64'))

        # Calculate the corrected nitrate concentration (uM) accounting for temperature and salinity and the pure
        # water calibration values.
        df['corrected_nitrate'] = ts_corrected_nitrate(dev.coeffs['cal_temp'], dev.coeffs['wl'], dev.coeffs['eno3'],
                                                       dev.coeffs['eswa'], dev.coeffs['di'], df['dark_value'],
                                                       df['temperature'], df['salinity'], channels,
                                                       df['measurement_type'], dev.coeffs['wllower'],
                                                       dev.coeffs['wlupper'])
    else:
        df['temperature'] = -9999.9
        df['salinity'] = -9999.9
        df['corrected_nitrate'] = -9999.9

    # convert the dataframe to a format suitable for the pocean OMTs, adding the deployment name
    df['deploy_id'] = deployment
    df = df2omtdf(df, lat, lon, depth)

    # add to the global attributes for the NUTNR
    attrs = SUNA
    attrs['global'] = dict_update(attrs['global'], {
        'comment': 'Mooring ID: {}-{}'.format(platform.upper(), re.sub('\D', '', deployment))
    })

    nc = OMTs.from_dataframe(df, outfile, attributes=attrs)
    nc.close()

    # re-open the netcdf file and add the raw channel measurements and the wavelengths with the additional dimension
    # of the measurement wavelengths.
    nc = Dataset(outfile, 'a')
    nc.createDimension('wavelengths', len(wavelengths))

    d = nc.createVariable('wavelengths', 'f', ('wavelengths',))
    d.setncatts(attrs['wavelengths'])
    d[:] = wavelengths

    d = nc.createVariable('channel_measurements', 'i', ('station', 't', 'wavelengths',))
    d.setncatts(attrs['channel_measurements'])
    d[:] = channels

    # synchronize the data with the netCDF file and close it
    nc.sync()
    nc.close()


if __name__ == '__main__':
    main()
