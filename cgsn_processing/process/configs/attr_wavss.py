#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@package cgsn_processing.process.configs.attr_wavss
@file cgsn_processing/process/configs/attr_wavss.py
@author Christopher Wingard
@brief Attributes for the AXYS Technologies wave monitoring sensor
"""
WAVSS = {
    'deploy_id': {
        'long_name': 'Deployment ID',
        'standard_name': 'deployment_id',
        'units': '1',
        'coordinates': 'time z longitude latitude',
        'grid_mapping': 'crs',
        'platform': 'platform',
        'ancillary_variables': 'platform',
        'coverage_content_type': 'physicalMeasurement'
    },
    'dcl_date_time_string': {
        'long_name': 'DCL Date and Time Stamp',
        'standard_name': 'dcl_date_time_string',
        'units': '1',
        'coordinates': 'time z longitude latitude',
        'grid_mapping': 'crs',
        'platform': 'platform',
        'ancillary_variables': 'platform',
        'coverage_content_type': 'physicalMeasurement'
    },
    'date_string': {},
    'time_string': {},
    'serial_number': {},
    'num_zero_crossings': {},
    'average_wave_height': {},
    'mean_spectral_period': {},
    'maximum_wave_height': {},
    'significant_wave_height': {},
    'significant_wave_period': {},
    'average_tenth_height': {},
    'average_tenth_period': {},
    'average_wave_period': {},
    'peak_period': {},
    'peak_period_read': {},
    'spectral_wave_height': {},
    'mean_wave_direction': {},
    'mean_directional_spread': {}
}
